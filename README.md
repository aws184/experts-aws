<h1 align="center">
  <img src="https://64.media.tumblr.com/bcdbfc92ed6807abee20aab41cf798c2/tumblr_muzpn0PcZq1sd4mmco1_500.gifv", width=300 height=330 alt="dohko logo">
</h1>


## Table of content

- [Sobre a Biblioteca](#sobre-o-jaiminho)
- [Funcionalidades Disponíveis](#funcionalidades-disponíveis)
- [Instalação](#instalação-do-pacote)
- [Exemplos de Uso](#utilização-prática)
- [Contatos](#contatos)

___

> Dohko de Libra (天秤座の童虎, Raibura no Dōko?) fora um lendário e renomado Cavaleiro de Ouro do Santuário de Atena por ter lutado e sobrevivido a Guerra Santa contra Hades no Século XVIII. Após o fim do conflito e ser incumbido por Atena de vigiar o selo que aprisionava o espectros do Imperador dos Mortos, Dohko ficou recluso nos Cinco Picos Antigos de Rozan até o Século XX, período que passara a ser venerado como Mestre Ancião (老師, Rōshi?). Como Cavaleiro de Libra, Dohko tem permissão de Atena para utilizar armas em combate, no entanto, ele só as utiliza em caso de extrema necessidade e por uma causa justa. ♎ [Saint Seiya Fandom](https://saintseiya.fandom.com/pt-br/wiki/Dohko_de_Libra#A_busca_da_cura_de_Seiya)

___

## Sobre a Biblioteca

Dohko é uma biblioteca Python inspirada no famoso Cavaleiro de Libra presente no anime [Saint Seiya](https://pt.wikipedia.org/wiki/Os_Cavaleiros_do_Zod%C3%ADaco) (ou Cavaleiros do Zodiado, no ocidente). Como guerreiro de Atena, sua habilidade em utilizar diferentes ferramentas de combate dentro das mais variadas ocasiões serviu como grande influência para o desenvolvimento de um pacote igualmente capaz de proporcionar tal experiência.

Dessa forma, o pacote `dohko` nasce como um conjunto de funcionalidades presentes em um repositório central com a responsabilidade de auxiliar Engenheiros de Dados e Analytics nas mais variadas tarefas do cotidiano. Utilizando diferentes abordagens para atacar diferentes problemas, o pacote conta com uma série de módulos específicos contendo soluções de fácil implementação dentro da dinâmica de atividades no universo de dados.

___

## Funcionalidades Disponíveis

As principais _features_ do pacote podem ser encontradas dentro dos seguintes módulos:

| Módulo | Descrição |
| :-- | :-- |
| `ingespy` | Módulo responsável por auxiliar o desenvolvimento local de processos de ingestão de dados através da criação automática de um conjunto de diretórios que podem ser utilizados para organizar o trabalho do Engenheiro, bem como o fornecimento de _templates_ e _scripts_ prontos para algumas das atividades mais comuns exigidas, como exemplos de conexão à servidores, queries úteis e execução de robôs de captura de arquivos. |
| `sampling` | Módulo criado com o objetivo de auxiliar na conexão com o ambiente Hadoop CDP e a subsequente extração de amostras de tabelas produtivas para serem utilizadas em diversos processos. Atualmente, uma das principais dores de Engenheiros de Dados e Analytics é a ausência de dados robustos em ambientes de desenvolvimento do Hadoop, impactando diretamente na entrega de produtos de dados, como ingestão e camadas SoT. Dessa forma, as funcionalidades deste módulo permitem que os usuários extraiam amostras de dados confiáveis do ambiente produtivo para acelerar a criação e o lançamento de produtos no ambiente de desenvolvimento. |

___

## Instalação

Os usuários podem utilizar as ferramentas desta louvável ~armadura~ biblitoeca através das seguintes maneiras:

1. Clonando o repositório e instalando o pacote em modo de edição
2. Instalando o pacote direto do repositório artifactory [EM ANÁLISE]

```bash
# Clonando repositório
git clone <url>

# Instalando pacote em modo de edição
cd dohko/
pip install -e .

# Instalando dependências do pacote
pip install -r requirements.txt

# Exemplo de funcionalidade
python scripts/start_project.py --name "dohko_sample_project" --path "../"
```

___

## Exemplos de Uso

### Iniciando a construção de um produto

Em muitos casos, uma boa organização do ambiente de trabalho facilita (e muito) o desenvolvimento de novas soluções. Pensando na grande pluralidade de atividades realizadas no cotidiano dos colaboradores, algumas funcionalidades foram consolidadas no `dohko` para proporcionar uma espécie de _guideline_ geral para um verdadeiro _quick start_ no desenvolvimento de produtos. Em linhas gerais, a utilização desta funcionalidade se resume nos seguintes tópicos:

- **Problema a ser resolvido:** usuários não possuem um padrão organizacional de diretórios para iniciar o desenvolvimento de um produto/projeto;
- **Solução implementada:** _script_ automatizado para a criação de diretórios no sistema operacional local de trabalho para contribuir com uma boa organização do produto/projeto a ser desenvolvido;
- **Como aplicar?** 🔽

```bash
# Criando conjunto de diretórios para o novo projeto
python dohko/scripts/ingespy.py --name "nome_do_projeto" --path "caminho_do_projeto"`
```

O _script_ `ingespy.py` é aplicado no ambiente local e seu resultado é dado pela criação automática de uma árvore de diretórios idealizada especialmente para proporcionar uma melhor experiência organizacional aos desenvolvedores e colaboradores. Para exemplificar suas vantagens, o bloco abaixo ilustra o retorno obtido através da execução do exemplo fornecido anteriormente.

